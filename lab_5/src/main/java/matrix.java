import java.util.Scanner;

public class matrix{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int m1 = in.nextInt();
        int n1 = in.nextInt();
        int[][] A = new int[m1][n1];
        for(int row=0;row<m1;row++){
            for(int col=0;col<n1;col++){
                A[row][col] = in.nextInt();
            }
        }
        int m2 = in.nextInt();
        int n2 = in.nextInt();
        int[][] B = new int[m2][n2];
        for(int row=0;row<m2;row++){
            for(int col=0;col<n2;col++){
                B[row][col] = in.nextInt();
            }
        }   

        if(n1 != m2){
            System.out.println("Tidak bisa dikalikan");
            return;
        }

        int[][] C = new int[m1][n2];
        for(int row=0;row<m1;row++){
            for(int col=0;col<n2;col++){
                int counter = 0;
                int entry = 0;
                while(counter<n1){
                    entry += A[row][counter]*B[counter][col];
                    counter += 1;
                }
                C[row][col] = entry;
            }
        }

        for(int row=0;row<m1;row++){
            for(int col=0;col<n2;col++){
                System.out.print((String.valueOf(C[row][col]))+" ");
            }
            System.out.println();
        }

    }
}