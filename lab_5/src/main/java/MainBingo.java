import java.util.Scanner;

public class MainBingo{
    
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int players = input.nextInt();
        String[] playerNames = new String[players];
        BingoCard[] playerBoard = new BingoCard[players];
        boolean finished = false;

        for(int i=0;i<players;i++){
            playerNames[i] = input.next();
        }
        for(int i=0;i<players;i++){
            Number[][] board = new Number[5][5];
            for(int row=0;row<5;row++){
                for(int col=0;col<5;col++){
                    board[row][col] = new Number(input.nextInt(),row,col);
                }
            }
            Number[] states = new Number[100];
            for(int r=0; r<5; r++){
			    for(int c=0; c<5; c++){				
				    states[board[r][c].getValue()] = board[r][c];
			    }
		    }
            input.nextLine();
            playerBoard[i] = new BingoCard(playerNames[i],board,states);
            System.out.println(); 
        }

        while(true){
            String cmd = input.next().toUpperCase();
            if(cmd.equals("INFO")){ 
                String target = input.next();
                for(int i=0;i<players;i++){
                    if(playerNames[i].equals(target)){
                        System.out.println(playerBoard[i].info());
                        break;
                    }
                }
            }
            else if(cmd.equals("RESTART")){
                String target = input.next();
                for(int i=0;i<players;i++){
                    if(playerNames[i].equals(target)){
                        if(playerBoard[i].getRestartCount()==0){
                            playerBoard[i].setRestartCount(playerBoard[i].getRestartCount()+1);
                            for(int k=0;k<players;k++){
                                playerBoard[k].restart();
                            }
                        }
                        else{
                            System.out.println(playerBoard[i].getName()+" sudah pernah mengajukan RESTART");
                        }
                        break;
                    }
                }
            }
            else if(cmd.equals("MARK")){
                int x = input.nextInt();
                for(int i=0;i<players;i++){
                    System.out.println(playerBoard[i].markNum(x));
                    playerBoard[i].checkBingo();
                    if(playerBoard[i].isBingo()) finished = true;
                }
            }
            else{
                System.out.println("Incorrect command");
            }
            
            if(finished) break;
            System.out.println();
        }
    }
}