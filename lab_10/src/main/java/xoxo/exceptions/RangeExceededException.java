package xoxo.exceptions;

/**
 * An exception that is thrown if the seed
 * that is used to decrypt/encrypt is out of range : 0-36.
 * 
 * @author Jonathan Christopher Jakub.
 */
public class RangeExceededException extends RuntimeException{

    public RangeExceededException(String message) {
        super(message);
    }

}