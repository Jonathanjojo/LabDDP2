package xoxo.exceptions;

/**
 * An exception that is thrown if the key
 * that is used to decrypt/encrypt contains characters that are
 * not alphabets or @ sign.
 * 
 * @author Jonathan Christopher Jakub.
 */
public class InvalidCharacterException extends RuntimeException{

    public InvalidCharacterException(String message) {
        super(message);
    }

}