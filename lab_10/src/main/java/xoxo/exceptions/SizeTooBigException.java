package xoxo.exceptions;

/**
 * An exception that is thrown if the message's size
 * exceeds 10 Kbit.
 * 
 * 
 * @author Jonathan Christopher Jakub.
 */
public class SizeTooBigException extends RuntimeException{

    public SizeTooBigException(String message) {
        super(message);
    }
    
}