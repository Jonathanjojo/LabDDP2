package xoxo.exceptions;

/**
 * An exception that is thrown if the
 * message/key field are not filled.
 * 
 * @author Jonathan Christopher Jakub.
 */
public class ZeroMessageKeyException extends RuntimeException{

    public ZeroMessageKeyException(String message) {
        super(message);
    }

}