package xoxo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import java.util.Arrays;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.ZeroMessageKeyException;
import xoxo.util.XoxoMessage;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Jonathan Christopher Jakub
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    private String path;
    private final String pathEncrypt = "encryption\\";
    private final String pathDecrypt = "decryption\\";

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui, String path) {
        this.gui = gui;
        this.path = path;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        this.gui.setVisible(true);
        
        this.gui.setEncryptFunction(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoMessage encrypted;
                String msg = gui.getMessageText();
                String key = gui.getKeyText();
                int seed = gui.getSeedInt();
                XoxoEncryption encryptor = new XoxoEncryption(key);
                try{
                    if (msg.length() == 0 || key.length() == 0) {
                        throw new ZeroMessageKeyException("Key and Message must not be empty");
                    }
                    if(gui.getSeedInt() == 0){
                        encrypted = encryptor.encrypt(msg);
                    } else {
                        encrypted = encryptor.encrypt(msg, seed);
                    }
                } catch (Exception ex) {
                    Exception exceptionError = (Exception) ex;
                    String mainMessage = "Message: " + exceptionError.getMessage();
                    String title = exceptionError.getClass().getName();
                    JOptionPane.showMessageDialog(
                        gui, 
                        mainMessage, 
                        title, 
                        JOptionPane.ERROR_MESSAGE);
                    return;
                }
                gui.appendLog(
                    "Encrypted message\t: " + encrypted.getEncryptedMessage() + "\n"
                    + "Decryption key\t\t: " + encrypted.getHugKey().getKeyString() + "\n"
                    + "Seed\t\t: " + encrypted.getHugKey().getSeed() + "\n"
                    );
                int counter = new File(path + pathEncrypt).listFiles().length + 1;
                try {
                    PrintWriter printer = new PrintWriter(path + pathEncrypt + counter + ".enc");
                    printer.println(encrypted.getEncryptedMessage());
                    printer.close();
                } catch (FileNotFoundException fnf){
                    JOptionPane.showMessageDialog(
                        gui, 
                        "Directory " + path + pathEncrypt + " not found. Please provide mentioned directory.",
                        "Destination Directory Not Found",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        this.gui.setDecryptFunction(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String decrypted;
                String msg = gui.getMessageText();
                String key = gui.getKeyText();
                int seed = gui.getSeedInt();
                XoxoDecryption decryptor = new XoxoDecryption(key);
                try{
                    if (msg.length() == 0 || key.length() == 0) {
                        throw new ZeroMessageKeyException("Key and Message must not be empty");
                    }
                    if(seed == 0){
                        decrypted = decryptor.decrypt(msg);
                    } else {
                        decrypted = decryptor.decrypt(msg, seed);
                    }
                } catch (Exception ex) {
                    Exception exceptionError = (Exception) ex;
                    String mainMessage = "Message: " + exceptionError.getMessage();
                    String title = exceptionError.getClass().getName();
                    JOptionPane.showMessageDialog(
                        gui, 
                        mainMessage, 
                        title, 
                        JOptionPane.ERROR_MESSAGE);
                    return;
                }
                gui.appendLog("Decrypted message\t: " + decrypted + "\n");
                int counter = new File(path + pathDecrypt).listFiles().length + 1;
                try {
                    PrintWriter printer = new PrintWriter(path + pathDecrypt + counter + ".txt");
                    printer.println(decrypted);
                    printer.close();
                } catch (FileNotFoundException fnf){
                    JOptionPane.showMessageDialog(
                        gui, 
                        "Directory " + path + pathDecrypt + " not found. Please provide mentioned directory.",
                        "Destination Directory Not Found",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}