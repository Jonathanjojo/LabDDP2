package xoxo;

import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.SwingConstants;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Jonathan Christopher Jakub.
 */
public class XoxoView extends JFrame{
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;


    /**
     * A field that used to be the input of the
     * seed for encrypting/decrypting.
     */
    private JTextField seedField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Label for message field.
     */
    private JLabel messageLabel;

    /**
     * Label for key field.
     */
    private JLabel keyLabel;

    /**
     * Label for seed field.
     */
    private JLabel seedLabel;

    /**
     * Label for log field.
     */
    private JLabel logLabel;


    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        this.setTitle("LAB 10 ! Encryptor - Decryptor");
        this.setBackground(Color.WHITE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.messageField = new JTextField();
        this.messageField.setPreferredSize(new Dimension( 250, 22 ));
        this.keyField = new JTextField();
        this.keyField.setPreferredSize(new Dimension( 250, 22 ));
        this.seedField = new JTextField();
        this.seedField.setPreferredSize(new Dimension( 250, 22 ));
        this.messageLabel = new JLabel("Message here : ");
        this.keyLabel = new JLabel("Key here : ");
        this.seedLabel = new JLabel("Seed here : ");
        this.decryptButton = new JButton("decrypt");
        this.decryptButton.setPreferredSize(new Dimension( 50, 22 ));
        this.encryptButton = new JButton("encrypt");
        this.encryptButton.setPreferredSize(new Dimension( 50, 22 ));
        this.logLabel = new JLabel("Log : ");
        this.logField = new JTextArea("");
        this.logField.setPreferredSize(new Dimension( 250, 100));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(this.messageLabel)
                .addComponent(this.keyLabel)
                .addComponent(this.seedLabel)
                .addComponent(this.logLabel))
            .addGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(this.messageField)
                .addComponent(this.keyField)
                .addComponent(this.seedField)
                .addComponent(this.logField))
            .addGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(this.encryptButton)
                .addComponent(this.decryptButton))
        );
       
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                .addComponent(this.messageLabel)
                .addComponent(this.messageField)
                .addComponent(this.encryptButton))
            .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                .addComponent(this.keyLabel)
                .addComponent(this.keyField)
                .addComponent(this.decryptButton))
            .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                .addComponent(this.seedLabel)
                .addComponent(this.seedField))
            .addGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(this.logLabel)
                .addComponent(this.logField))
        );

        this.pack();
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the seed field.
     * 
     * @return The input seed string.
     */
    public int getSeedInt() {
        try {
            return Integer.parseInt(seedField.getText());
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
        this.pack();
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}