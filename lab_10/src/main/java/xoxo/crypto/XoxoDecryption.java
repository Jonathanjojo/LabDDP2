package xoxo.crypto;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * Default seed that is used to decrypt.
     */
    private static final int DEFAULT_SEED = 18;

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage) {
        String decryptedMessage = "";
        for (int i = 0; i < encryptedMessage.length(); i++) {
            int m = encryptedMessage.charAt(i);
            int k = this.hugKeyString.charAt(i % this.hugKeyString.length());
            int value = m ^ ((k ^ this.DEFAULT_SEED) - 'a');
            decryptedMessage += (char) value;
        }
        return decryptedMessage;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @param seed seed to decrypt the message.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        String decryptedMessage = "";
        for (int i = 0; i < encryptedMessage.length(); i++) {
            int m = encryptedMessage.charAt(i);
            int k = this.hugKeyString.charAt(i % this.hugKeyString.length());
            int value = m ^ ((k ^ seed) - 'a');
            decryptedMessage += (char) value;
        }
        return decryptedMessage;
    }
}