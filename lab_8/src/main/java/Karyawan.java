import java.util.ArrayList;
import java.util.HashMap;

/**
* Abstract Class : Karyawan.
* @author  Jonathan Christopher Jakub.
* @since   4-17-2018.
*/
public abstract class Karyawan{
    private String nama;
    private double gaji;
    private ArrayList<Karyawan> bawahan = new ArrayList<Karyawan>();
    private int counterGaji;

    /**
     * Setters and Getters.
     */
    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public int getJumlahBawahan(){
        return this.bawahan.size();
    }

    public ArrayList<Karyawan> getBawahan(){
        return this.bawahan;
    }

    public double getGaji(){
        return this.gaji;
    }

    public void setGaji(double gaji){
        this.gaji = gaji;
    }

    /**
     * Method to add subordinates.
     * @param bawahan as the subordinates to be taken.
     * @return output string result.
     */
    public String tambahBawahan(Karyawan bawahan){
        if(bisaJadiBawahan(bawahan)){
            this.bawahan.add(bawahan);
            return "Karyawan " + bawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + this.getNama();
        } 
        return "Anda tidak layak memiliki bawahan";
    }

    /**
     * helper method to determine the subordination allowance. 
     * @param bawahan as the subordinates to be taken.
     * @return boolean : allowed/not allowed.
     */
    protected abstract boolean bisaJadiBawahan(Karyawan bawahan);

    /**
     * helper method to keep count the income freq. 
     * @return int : how many times this employee has received income.
     */
    public int addedCounterGaji(){
        this.counterGaji += 1;
        return this.counterGaji;
    }


    public void printBawahan(){
        HashMap<Integer, String> pangkat = new HashMap<>();
        pangkat.put(1, "Intern");
        pangkat.put(2, "Staff");
        pangkat.put(3, "Manager");
        this.bawahan.sort((b1, b2) -> b1.getClass().equals(b2.getClass()) ? b1.getNama().compareTo(b2.getNama()) : Rank(b1).compareTo(Rank(b2)));
        System.out.println("Bawahan "+this.getNama()+" :");
        for(Karyawan k: this.bawahan){
            System.out.println(k.getNama() + " " + pangkat.get(Rank(k)));
        }
    } 

    private static Integer Rank(Karyawan k){
        if (k instanceof Manager){
            return 3;
        } else if (k instanceof Staff){
            return 2;
        } else if (k instanceof Intern){
            return 1;
        } else {
            return 0;
        }
    }

}