/**
* SubClass : Intern.
* child of Karyawan.
* @author  Jonathan Christopher Jakub.
* @since   4-17-2018.
*/

public class Intern extends Karyawan{
    /**
     * This method is Intern constructor.
     * @param nama string of the name of the intern.
     * @param gaji double income of the intern.
     */
    public Intern(String nama, double gaji){
        this.setNama(nama);
        this.setGaji(gaji);
    }

    /**
     * helper method to determine the subordination allowance. 
     * Implement the abstract method from Karyawan.
     * @param bawahan as the subordinates to be taken.
     * @return boolean : allowed/not allowed.
     */
    protected boolean bisaJadiBawahan(Karyawan bawahan){
        return false;
    }
}