import java.util.ArrayList;
import java.util.Iterator;

/**
* Class : Korporasi.
* @author  Jonathan Christopher Jakub.
* @since   4-17-2018.
*/
public class Korporasi{
    private static final int BATAS_PEKERJA = 1000;
    private ArrayList<Karyawan> pekerja = new ArrayList<Karyawan>();
    private double BATAS_GAJI = 18000;
    private String nama;

    /**
     * This method is Korporasi constructor.
     * @param nama string of the name of the corporatioin.
     */
    public Korporasi(String nama){
        this.nama = nama;
    }

    /**
     * Method to find employee.
     * @param nama String name of the employee.
     * @return employee found (or null).
     */
    private Karyawan findKaryawan(String nama){
        for(Karyawan k:this.pekerja){
            if(k.getNama().equals(nama)){
                return k;
            }
        } 
        return null;
    }

    /**
     * Method to add employee.
     * @param nama String name of the employee.
     * @param jabatan String position/rank of the employee.
     * @param gaji double income of the employee.
     * @return String result.
     */
    public String tambahKaryawan(String nama, String jabatan, String gaji){
        if(findKaryawan(nama) == null){
            if(jabatan.toUpperCase().equals("MANAGER")){
                this.pekerja.add(new Manager(nama,Double.parseDouble(gaji)));
            } else if(jabatan.toUpperCase().equals("STAFF")){
                this.pekerja.add(new Staff(nama,Double.parseDouble(gaji)));
            } else {
                this.pekerja.add(new Intern(nama,Double.parseDouble(gaji)));
            }
            return nama +" mulai bekerja sebagai " + jabatan + " di " + this.getNama();
        } 
        return "Karyawan dengan nama " + nama + " telah terdaftar";
    }
    
    /**
     * Method to show the status of an employee.
     * @param nama String name of the employee.
     * @return String status of the employee.
     */
    public String status(String nama){
        try{
            return this.findKaryawan(nama).getNama() + " " + (int)(this.findKaryawan(nama).getGaji());
        } catch (NullPointerException e){
            return "Karyawan tidak ditemukan";
        }
    }

    /**
     * Method to distribute incomes to all employees.
     * @return String output.
     */
    public String gajian(){
        String promosi = "";
        String naikgaji = "";
        ArrayList<Karyawan> copy = new ArrayList<>();
        for(Karyawan k : this.getKaryawan()){
            copy.add(k);
        }
        for(Karyawan k: this.getKaryawan()){
            if(k.addedCounterGaji()%6 == 0){
                naikgaji += this.naikGaji(k);
            }
        }
        for (Karyawan k: copy){
            if(k instanceof Staff){
                if(((Staff)k).getGaji() > (this.getBatasGaji())){
                    promosi += this.promosi(k);
                }
            }
        }
        return "Semua karyawan telah diberi gaji" + naikgaji + promosi;
    }
    /**
     * Method to raise income of an employee.
     * @param k Issued employee.
     */
    private String naikGaji(Karyawan k){
        String out = "\n" + k.getNama() + " mengalami kenaikan gaji sebesar 10% dari ";
        out += (int)(k.getGaji());
        k.setGaji(k.getGaji()*1.1);
        out += " menjadi " + (int)(k.getGaji());
        return out;
    }
    
    /**
     * Method to promote a Staff to a Manager
     * @param k Issued staff.
     */
    private String promosi(Karyawan k){
        Karyawan managerBaru = new Manager(k.getNama(),k.getGaji());
        for(Karyawan b:k.getBawahan()){
            managerBaru.tambahBawahan(b);
        }
        for(Karyawan a: this.getKaryawan()){
            Iterator<Karyawan> iter = a.getBawahan().iterator();
            while(iter.hasNext()){
                if(iter.next() == k){
                    iter.remove();
                }
            }
        }

        this.getKaryawan().add(managerBaru);
        this.getKaryawan().remove(k);



        return "\nSelamat, " + managerBaru.getNama() + " telah dipromosikan menjadi MANAGER";
    }
    
    /**
     * Method to add subordinate for an employee.
     * @param bawahan String name of the to-be-subordinate.
     * @param atasan String name of the to-be-superordinate.
     * @return String result.
     */
    public String tambahBawahan(String atasan, String bawahan){
        try {
            Karyawan a = this.findKaryawan(atasan);
            Karyawan b = this.findKaryawan(bawahan);
            return a.tambahBawahan(b);
        } catch (NullPointerException e){
            return "Nama tidak berhasil ditemukan";
        }
    }

    /**
     * Getter for corporate's employees list
     */
    public ArrayList<Karyawan> getKaryawan(){
        return this.pekerja;
    }

    /**
    * Method to set max income before get promoted.
    * @param int batas as the threshold.
    */
    public String setBatasGaji(double batas){
        this.BATAS_GAJI= batas;
        return "Batas gaji staff ditetapkan: " + (int)batas;
    }


    /**
    * Method to get max income before get promoted.
    */
    public double getBatasGaji(){
        return this.BATAS_GAJI;
    }

    /**
    * Method to get the name of this corporatioin.
    */
    public String getNama(){
        return this.nama;
    }

    public void printBawahan(String nama){
        findKaryawan(nama).printBawahan();
    }
}