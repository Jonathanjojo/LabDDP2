/**
* SubClass : Manager.
* child of Karyawan.
* @author  Jonathan Christopher Jakub.
* @since   4-17-2018.
*/
public class Manager extends Karyawan{
    private static final int BATAS_BAWAHAN = 10;
    
    /**
     * This method is Manager constructor.
     * @param nama string of the name of the manager.
     * @param gaji double income of the manager.
     */
    public Manager(String nama, double gaji){
        this.setNama(nama);
        this.setGaji(gaji);
    }

    /**
     * helper method to determine the subordination allowance. 
     * Implement the abstract method from Karyawan.
     * @param bawahan as the subordinates to be taken.
     * @return boolean : allowed/not allowed.
     */
    protected boolean bisaJadiBawahan(Karyawan bawahan){
        if((bawahan instanceof Staff || bawahan instanceof Intern) 
            && this.getJumlahBawahan() <= BATAS_BAWAHAN) {
            return true;
        } 
        return false;
    }
}