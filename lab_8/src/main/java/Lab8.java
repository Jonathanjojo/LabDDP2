import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
* Class : Lab8.
* main class untuk lab8
* @author  AFFANDHIA, RICKY, modified by : Jonathan Christopher Jakub.
* @since   4-17-2018.
*/
class Lab8 {
    /**
    * This is the main method which makes use of the class: Korporasi
    * @param args Unused.
    */
    public static void main(String[] args) {
        File inputFile = new File("input.txt");
        try{
            Scanner in = new Scanner(inputFile);
            Korporasi TAMPAN = new Korporasi("TAMPAN");
            while(in.hasNextLine()){
                if (in.hasNextDouble()){
                    System.out.println(TAMPAN.setBatasGaji(Double.parseDouble(in.nextLine())));
                } else {
                    String[] cmd = in.nextLine().split(" ");
                    if(cmd[0].equals("TAMBAH_KARYAWAN")){
                        System.out.println(TAMPAN.tambahKaryawan(cmd[1], cmd[2], cmd[3]));
                    } else if(cmd[0].equals("STATUS")){
                        System.out.println(TAMPAN.status(cmd[1]));
                    } else if(cmd[0].equals("TAMBAH_BAWAHAN")){
                        System.out.println(TAMPAN.tambahBawahan(cmd[1],cmd[2]));
                    } else if (cmd[0].equals("GAJIAN")){
                        System.out.println(TAMPAN.gajian());
                    } else if (cmd[0].equals("PRINT_BAWAHAN")) {
                        TAMPAN.printBawahan(cmd[1]);
                    } else {
                        System.out.println("Perintah tidak dikenal");
                    }
                }   
            }
            in.close();
        } catch (FileNotFoundException e){
            System.out.print("File tidak ditemukan");
        }
    }
}