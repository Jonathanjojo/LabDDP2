import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Jonathan Christopher Jakub, NPM 1706040151, Kelas DDP2-D, GitLab Account: @Jonathanjojo
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);

		// User Interface untuk meminta masukan
		String nama = "";
		String alamat = "";
		short panjang = 0;
		short lebar = 0;
		short tinggi = 0;
		float berat = 0;
		byte anggotaKeluarga = 0;
		String tanggalLahir = "";
		String catatan = "";
		byte jumlahCetakan = 0;
		//Input
		try{
			System.out.print("\nPROGRAM PENCETAK DATA SENSUS\n" +
					"----------------------------\n" +
					"Nama Kepala Keluarga   : ");
			nama = input.nextLine();
			System.out.print("Alamat Rumah           : ");
			alamat = input.nextLine();
			System.out.print("Panjang Tubuh (cm)     : ");
			panjang = input.nextShort();
			if (panjang<=0 || panjang>250){throw new java.util.InputMismatchException("");}
			System.out.print("Lebar Tubuh (cm)       : ");
			lebar = input.nextShort();
			if (lebar<=0 || lebar>250){throw new java.util.InputMismatchException("");}
			System.out.print("Tinggi Tubuh (cm)      : ");
			tinggi =input.nextShort();
			if (tinggi<=0 || tinggi>250){throw new java.util.InputMismatchException("");}
			System.out.print("Berat Tubuh (kg)       : ");
			berat = input.nextFloat();
			if (berat<=0 || berat>150){throw new java.util.InputMismatchException("");}
			System.out.print("Jumlah Anggota Keluarga: ");
			anggotaKeluarga = input.nextByte();
			if(anggotaKeluarga<=0 || anggotaKeluarga>20){throw new java.util.InputMismatchException("");}
			input.nextLine();
			System.out.print("Tanggal Lahir          : ");
			tanggalLahir = input.nextLine();
			if (!(tanggalLahir.matches("\\d{2}-\\d{2}-\\d{4}"))){throw new java.util.InputMismatchException("");}
			System.out.print("Catatan Tambahan       : ");
			catatan = input.nextLine().trim();
			System.out.print("Jumlah Cetakan Data    : ");
			jumlahCetakan = input.nextByte();
			input.nextLine();
		}
		// Menangkap error input
		catch (java.util.InputMismatchException e){
			System.out.println("\nWARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
 
		//Hitung rasio berat per volume
		float volume = (float)0.000001*panjang*lebar*tinggi;
		int rasio = (int)((berat)/(volume));

		// Mengecek ada atau tidaknya catatan
		if (catatan.length() == 0) {catatan = "Tidak ada catatan tambahan";}
		else {catatan = "Catatan: "+ catatan;}		

		//Mencetak cetakan sesuai permintaan
		for (byte cetakan=0;cetakan<jumlahCetakan;cetakan++) {
			//Input nama penerima hasil cetak data
			System.out.print("\nPencetakan " + (cetakan+1) + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			System.out.println("\nDATA SIAP DICETAK UNTUK "+penerima);			
			for(int underscore=0;underscore<(24+penerima.length());underscore++){
				System.out.printf("-");
			}

			//Cetak hasil input data
			String hasil = "\n";
			hasil+= nama + " - " + alamat + "\n" +
					"Lahir pada tanggal " + tanggalLahir + "\n" +
					"Rasio Berat Per Volume = " + String.valueOf(rasio) + " kg/m^3\n" +
					catatan; 
			System.out.println(hasil);
		}

		//Membuat nomor keluarga
		int kalkulasi = panjang*lebar*tinggi;
		for(byte i=0;i<nama.length();i++){
			kalkulasi += (int)nama.charAt(i);
		}
		kalkulasi %= 10000;
		String nomorKeluarga = nama.charAt(0)+String.valueOf(kalkulasi);

		// Hitung anggaran makanan per tahun 
		int anggaran = (50000)*(365)*(anggotaKeluarga);

		// Hitung umur kepala keluarga
		short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]); 
		short umur = (short)(2018-(tahunLahir));

		//Proses klasifikasi apartemen
		String namaApartemen = "";
		String kabupatenApartemen = "";
		if(umur>=0 && umur<=18){
			namaApartemen = "PPMT";
			kabupatenApartemen = "Rotunda";
		}		
		else if(umur>=19 && umur<=1018 && anggaran>=0 && anggaran<=100000000){
			namaApartemen = "Teksas";
			kabupatenApartemen = "Sastra";
		}
		else if(umur>=19 && umur<=1018 && anggaran>100000000){
			namaApartemen = "Mares";
			kabupatenApartemen = "Margonda";
		}

		//Output rekomendasi
		String rekomendasi = "\n";
		rekomendasi +=  "REKOMENDASI APARTEMEN\n" +
						"---------------------\n" +
						"MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + "\n" +
						"MENIMBANG : Anggaran makanan tahunan: Rp " + String.valueOf(anggaran) + "\n" +
						"            Umur kepala keluarga: " + String.valueOf(umur) + " tahun\n" +
						"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
						namaApartemen + ", kabupaten " + kabupatenApartemen;
		System.out.println(rekomendasi);

		input.close();
	}
}



