package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.text.SimpleDateFormat;  
import java.util.ArrayList;
import java.util.Date;
import java.math.BigInteger;
import java.text.ParseException;

/**
* Class representing event managing system.
*/
public class EventSystem
{
    /** List of events */
    private ArrayList<Event> events;
    
    /** List of users */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
    /**
    * Adds a new event to the event system that can be attended by user.
    * @param name name of the event.
    * @param startTimeStr start time of the event.
    * @param endTimeStr end time of the event.
    * @param costStr cost of the event.
    * @return string output of the coresponding action.
    */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costStr) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        try {
            Date startTime = formatter.parse(startTimeStr);
            Date endTime = formatter.parse(endTimeStr);
            BigInteger cost = new BigInteger(costStr);
            if (!startTime.before(endTime)) {
                return "Waktu yang diinputkan tidak valid!";
            } else {
                if (this.getEvent(name) == null) {
                    this.events.add(new Event(name, startTime, endTime, cost));
                    return "Event " + name + " berhasil ditambahkan!";
                } else {
                    return "Event " + name + " sudah ada!";
                }
            }
        } catch (ParseException e) {
            return "Waktu yang diinputkan tidak valid!";
        }
        
    }
    
    /**
    * Adds a new user to this event system.
    * @param name name of the new user.
    * @return String output of the coresponding action.
    */
    public String addUser(String name)
    {
        if(this.getUser(name) == null) {
            this.users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
    }
    
    /**
    * Register a user instance to attend an event.
    * Return not found String if searched event and/or user not found.
    * Return string output : user can attend the event or not based on
    * the event is overlapping with any of currently planned event or not. 
    * 
    * @param userName name of the user willing to attend event.
    * @param eventName name of the event the user wants to attend.
    * @return String output of the coresponding action.
    */
    public String registerToEvent(String userName, String eventName)
    {
        User user = this.getUser(userName);
        Event event = this.getEvent(eventName);
        String out = "";
        if (user == null && event == null) {
            return "Tidak ada pengguna dengan nama " + userName 
                + " dan acara dengan nama " + eventName + "!";
        } else if (user == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (event == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        } else {
            if (user.addEvent(event)) {
                return user.getName() + " berencana menghadiri " + event.getName() + "!";
            } else {
                return user.getName() + " sibuk sehingga tidak dapat menghadiri " + event.getName() + "!";
            }
        }
    }

    /**
    * Returns the user object with the searched name.
    *
    * @param name name of the searched user.
    * @return searched user object, null if not found.
    */
    public User getUser(String name) {
        for(User u: this.users) {
            if (u.getName().equals(name)){
                return u;
            }
        }
        return null;
    }

    /**
    * Returns the event object with the searched name.
    *
    * @param name name of the searched event.
    * @return searched event object, null if not found.
    */
    public Event getEvent(String name) {
        for(Event e: this.events) {
            if (e.getName().equals(name)) {
                return e;
            }
        }
        return null;
    }

}