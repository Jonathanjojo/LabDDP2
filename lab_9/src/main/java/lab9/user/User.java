package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    * @param name Name of the user
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return this.name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    * @param newEvent event object the user willing to attend
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        if(!this.overlappingEvent(newEvent)) {
            this.events.add(newEvent);
            return true;
        }
        return false;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> copyOfEvents = new ArrayList<Event>(this.events);
        copyOfEvents.sort((e1, e2) -> e1.getStartTime().compareTo(e2.getStartTime()));
        return copyOfEvents;
    }

    /**
    * Helper method to determine the event is overlapping with any already planned events
    *
    * @return boolean the event is overlapping, true is overlapping, false otherwise
    */
    private boolean overlappingEvent(Event event) {
        for(Event e: this.events) {
            if (event.overlaps(e)) {
                return true;
            }
        }
        return false;
    }

    /**
    * Returns the total cost of all events attended by this user.
    *
    * @return total cost of all events.
    */
    public BigInteger getTotalCost() {
        BigInteger total = new BigInteger("0");
        for(Event e: this.events) {
            total = total.add(e.getCost());
        }
        return total;
    }
    
}
