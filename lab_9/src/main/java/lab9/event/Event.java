package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date; 

/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;

    /** Start time of event */
    private Date startTime;

    /** End time of event */
    private Date endTime;

    /** Cost for attending the event */
    private BigInteger cost;
    
    public Event(String name, Date startTime, Date endTime, BigInteger cost) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.cost = cost;
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance.
    */
    public String getName() {
        return this.name;
    }

    /**
    * Accessor for cost field. 
    * @return cost of this event instance.
    */
    public BigInteger getCost() {
        return this.cost;
    }

    /**
    * Accessor for start time field. 
    * @return start time of this event instance.
    */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
    * Accessor for end time field. 
    * @return end time of this event instance.
    */
    public Date getEndTime() {
        return this.endTime;
    }
    
    /**
     * Returns the attribute list of this event instance in String:
     * Start time, End time, Cost.
     */
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return this.name + "\n"
            + "Waktu mulai: " + formatter.format(this.startTime) + "\n"
            + "Waktu selesai: " + formatter.format(this.endTime) + "\n"
            + "Biaya kehadiran: " + this.cost;
    }

    /**
    * Helper method to determine the event is overlapping with another event 
    *
    * @return boolean the event is overlapping, true is overlapping, false otherwise
    */
    public boolean overlaps(Event event) {
        
        if ((event.getStartTime().before(this.getStartTime()) && event.getEndTime().after(this.getStartTime())) 
            || (event.getStartTime().before(this.getEndTime()) && event.getEndTime().after(this.getEndTime())) 
            || (event.getStartTime().before(this.getStartTime()) && event.getEndTime().after(this.getEndTime()))
            || (event.getStartTime().equals(this.getStartTime()) || event.getEndTime().equals(this.getEndTime()))
            || (event.getStartTime().after(this.getStartTime()) && event.getEndTime().before(this.getEndTime()))) {
            return true;
        }
        return false;
    }
}