package theater;

//importing packages and modules
import customer.Customer;
import movie.Movie;
import ticket.Ticket;
import java.util.ArrayList;
import java.util.Arrays;

//class Theater
public class Theater {
    private String name;
    private long balance;
    private ArrayList<Ticket> tickets = new ArrayList<Ticket>();
    private Movie[] movies;

    //constructor for Theater
    public Theater(String name, long balance, ArrayList<Ticket> tickets, Movie[] movies){
        this.name = name;
        this.balance = balance;
        this.tickets = tickets;
        this.movies = movies;
    }

    //setters
    public void setBalance(long balance){
        this.balance = balance;
    }
    public void setTicket(ArrayList<Ticket> tickets){
        this.tickets = tickets;
    }
    public void setMovie(Movie[] movies){
        this.movies = movies;
    }

    //getters
    public String getName(){
        return this.name;
    }
    public long getBalance(){
        return this.balance;
    }
    public ArrayList<Ticket> getTickets(){
        return this.tickets;
    }
    public Movie[] getMovies(){
        return this.movies;
    }

    //method printInfo of a theater, return void
    public void printInfo(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop\t\t\t\t\t: "+this.getName());
        System.out.println("Saldo Kas\t\t\t\t: "+this.getBalance());
        System.out.println("Jumlah tiket tersedia\t: "+this.getTickets().size());
        System.out.print("Daftar Film tersedia\t: ");
        for(int f=0;f<this.getMovies().length-1;f++){
            System.out.print(this.getMovies()[f].getTitle()+", ");
        }
        System.out.println(this.getMovies()[this.getMovies().length-1].getTitle());
        System.out.println("------------------------------------------------------------------");

    }

    //method print total balance of all theaters, void
    public static void printTotalRevenueEarned(Theater[] theaters){
        String out = "";
        long total = 0;
        for(int f=0;f<theaters.length;f++){
            total += theaters[f].getBalance();
            out +=  "Bioskop        : "+theaters[f].getName()+"\n"
                +   "Saldo Kas      : Rp. "+theaters[f].getBalance() +"\n\n";
        }
        System.out.println(
            "Total uang yang dimiliki Koh Mas : Rp. "+total+"\n"
            + "------------------------------------------------------------------\n"
            + out 
            + "------------------------------------------------------------------");
    }
}