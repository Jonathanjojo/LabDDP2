package movie;

//importing packages
import customer.Customer;
import theater.Theater;
import ticket.Ticket;

//class Movie
public class Movie{
    private String title;
    private String genre;
    private int duration;
    private String rating;
    private String region;

    //constructor
    public Movie (String title, String rating, int duration, String genre, String region){
        this.title = title;
        this.rating = rating;
        this.duration = duration;
        this.genre = genre;
        this.region = "Film " + region;
    }

    //getters
    public String getTitle(){
        return this.title;
    }
    public String getGenre(){
        return this.genre;
    }
    public int getDuration(){
        return this.duration;
    }
    public String getRating(){
        return this.rating;
    }
    public String getRegion(){
        return this.region;
    }

    //method printInfo of a movie, void
    public String printInfo(){
        return ("------------------------------------------------------------------\n"+
        "Judul\t: "+this.getTitle() + "\n" +
        "Genre\t: "+this.getGenre() + "\n" +
        "Durasi\t: "+this.getDuration() + " menit\n" +
        "Rating\t: "+this.getRating() + "\n" +
        "Jenis\t: "+this.getRegion() + "\n" +
        "------------------------------------------------------------------");
    }
}