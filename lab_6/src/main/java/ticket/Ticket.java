package ticket;

//importing packages
import customer.Customer;
import movie.Movie;
import theater.Theater;

//class ticket
public class Ticket{
    private Movie movie;
    private String schedule;
    private boolean btype;
    private String type;
    private boolean used;
    private Customer owner;
    private long price;

    //constructor
    public Ticket (Movie movie, String schedule, boolean btype){
        this.movie = movie;
        this.schedule = schedule;
        this.btype = btype;
        this.used = false;
        this.owner = null;
        if(schedule.equals("Sabtu")||schedule.equals("Minggu")){
            this.price = 100000;
        }
        else{
            this.price = 60000;
        }
        if(this.btype == true){
            this.price *= 1.2;
            this.type = "3 Dimensi";
        }
        else{
            this.type = "Biasa";
        }
    }

    //getters
    public Movie getMovie(){
        return this.movie;
    }
    public String getSchedule(){
        return this.schedule;
    }
    public String getType(){
        return this.type;
    }
    public boolean getUsed(){
        return this.used;
    }

    public long getPrice(){
        return this.price;
    }

    public Customer getOwner(){
        return this.owner;
    }

    //setters
    public void setUsed(boolean used){
        this.used = used;
    }

    public void setOwner(Customer owner){
        this.owner = owner;
    }

    //method printInfo of the movie coresponding with this ticket, void
    public void printInfo(){
        System.out.println("Film			: "+this.getMovie().getTitle());
        System.out.println("Jadwal Tayang	: "+this.getSchedule());
        System.out.println("Jenis			: "+this.getType());
    }
}