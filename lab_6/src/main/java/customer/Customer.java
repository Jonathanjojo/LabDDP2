package customer;

//importing packages and modules
import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import java.util.ArrayList;
import java.util.Arrays;

//class Customer
public class Customer{
    private String name;
    private boolean gender;
    private int age;

    //constructor
    public Customer(String name, boolean gender, int age){
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    //getters
    public String getName(){
        return this.name;
    }
    public boolean getGender(){
        return this.gender;
    }
    public int getAge(){
        return this.age;
    }

    //print requested movie info if exist, void
    public void findMovie(Theater theater, String title){
        boolean found = false;
        for(int f=0;f<theater.getMovies().length;f++){
            if(theater.getMovies()[f].getTitle().equals(title)){
                found = true;
                System.out.println(theater.getMovies()[f].printInfo());
                break;
            }
        }
        if(!found){
            System.out.println("Film "+ title +" yang dicari " + this.getName() + " tidak ada di bioskop "+
                theater.getName());
        }
    }

    //return ticket ordered if still available
    public Ticket orderTicket(Theater theater, String title, String schedule, String type){
        for(int t=0; t<theater.getTickets().size();t++){
            boolean allowed = false;
            if(
            (theater.getTickets().get(t).getMovie().getTitle().equals(title))&&
            (theater.getTickets().get(t).getSchedule().equals(schedule))&&
            (theater.getTickets().get(t).getType().equals(type))){
                allowed = true;
            }

            if(allowed){
                if ((theater.getTickets().get(t).getMovie().getRating().equals("Remaja") && this.getAge()<13)
                    ||(theater.getTickets().get(t).getMovie().getRating().equals("Dewasa") && this.getAge()<17))
                    {
                        allowed = false;
                        System.out.println(this.getName()+" masih belum cukup umur untuk menonton "+ 
                            theater.getTickets().get(t).getMovie().getTitle() + " dengan rating " +
                            theater.getTickets().get(t).getMovie().getRating());
                        return null ;
                    }
                else{
                    System.out.println(this.getName()+" telah membeli tiket "+
                        theater.getTickets().get(t).getMovie().getTitle()+ " jenis " + 
                        theater.getTickets().get(t).getType()+ " di " +
                        theater.getName() + " pada hari " +
                        theater.getTickets().get(t).getSchedule() + " seharga Rp. "+
                        theater.getTickets().get(t).getPrice());
                    theater.getTickets().get(t).setOwner(this);
                    theater.setBalance(theater.getBalance()+theater.getTickets().get(t).getPrice());
                    
                    return theater.getTickets().get(t);
                }
            }
        }
        System.out.println("Tiket untuk film "+ title + " jenis " + type + 
            " dengan jadwal " + schedule + " tidak tersedia di " + theater.getName());
        return null;
    }

    //watch movie coresponding with the ticket
    public void watchMovie(Ticket ticket){
        ticket.setUsed(true);
        System.out.println(this.getName()+" telah menonton "+ticket.getMovie().getTitle());   
    }

    //cancel ticket, ask refund to requested theater if still refundable
    public void cancelTicket(Theater theater){
        boolean found = false;
        for(int i=theater.getTickets().size()-1;i>0;i--){
            if(theater.getTickets().get(i).getOwner()==this){
                if(theater.getTickets().get(i).getUsed()){
                    System.out.println("Tiket tidak bisa dikembalikan karena film " 
                        + theater.getTickets().get(i).getMovie().getTitle() 
                        + " sudah ditonton oleh " + this.getName());
                }
                else{
                    if(theater.getBalance()-theater.getTickets().get(i).getPrice()<0){
                        System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop " 
                            + theater.getName() + " lagi tekor...");
                    }
                    else{
                        theater.getTickets().get(i).setOwner(null);
                        theater.setBalance(theater.getBalance() - theater.getTickets().get(i).getPrice());
                        System.out.println("Tiket film " +
                            theater.getTickets().get(i).getMovie().getTitle() + " dengan waktu tayang "+
                            theater.getTickets().get(i).getSchedule() + " jenis " + 
                            theater.getTickets().get(i).getType() + " dikembalikan ke bioskop " + 
                            theater.getName());
                        found = true;
                    }
                }
            }
        }
        if(!found){
            System.out.println("Tiket tidak ditemukan");
        }
    }
}