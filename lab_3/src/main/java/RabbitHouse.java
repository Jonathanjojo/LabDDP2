//@author Jonathan Christopher Jakub, NPM 1706040151, Kelas DDP2-D, GitLab Account: @Jonathanjojo
//RabbitHouse

//importing modules
import java.util.Scanner;

public class RabbitHouse {
	public static long answer[] = new long[1005];
	//main function
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean palindromitis = true;
		//user input
		String strType = input.next();
		if(strType.toLowerCase().equals("normal")) palindromitis = false;
		else if (strType.toLowerCase().equals("palindrom")) palindromitis = true;
		else{
			System.out.println("Tipe tidak dikenali");
			System.exit(0);
		} 
		String strRabbit = input.next().trim(); 
		//recurssion called for calculating all the offsprings of the rabbit
		int n = breed(strRabbit.toLowerCase(),palindromitis);
		if(!Palindrome(strRabbit.toLowerCase()) || !palindromitis) n++;
		//output
		System.out.println(n); 
		for (int i = 0; i < 1005; i++) answer[i] = -1;
		System.out.println(fibo(30));
	}

	static int breed(String strRabbit, boolean palindromitis){
		int intBreed = 0;
		System.out.println(strRabbit);
		//base case
		if(strRabbit.length()<=1){
			return 0;
		}
		else{
			//checking "palindromitis" disease (condition)
			if(!palindromitis || (!Palindrome(strRabbit) && palindromitis)){
				for(int chr=0;chr<strRabbit.length();chr++){
					//breeding: getting offsprings
					String newBreed = (strRabbit.substring(0,chr)+strRabbit.substring(chr+1,strRabbit.length())).trim()	;
					if(!palindromitis || (!Palindrome(newBreed) && palindromitis)){
						//counting breeds
						intBreed += 1 + breed(newBreed,palindromitis);
					}
				}
				return intBreed;
			}
			//in case the breeder dies
			else return 0;
		}
	}
	//Checking palindrome
	static boolean Palindrome(String str){
		str = str.toLowerCase();
		if (str.length()<=1)
			return true;
		else{
			 if(str.charAt(0) == str.charAt(str.length()-1)){
			 	return Palindrome(str.substring(1, str.length()-1));
			 }
			 else return false;
		}
	}



	public static long fibo(int n){
		if(n<=1) return 1;
		if (answer[n] != -1) return answer[n];
		answer[n] = fibo(n-1) + fibo(n-2);
		return answer[n];
	}
}