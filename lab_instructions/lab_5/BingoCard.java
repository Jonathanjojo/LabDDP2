/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
    private String name;
    private int restartCount;
    private String colon;
	
	//constructor 1
	public BingoCard(String name, Number[][] numbers, Number[] numberStates) {
        this.name = name;
        this.colon = ": ";
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
        this.restartCount = 0;
	}
	//constructor 2
    public BingoCard(Number[][] numbers, Number[] numberStates) {
        this.name = "";
        this.colon = "";
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
        this.restartCount = 0;
	}
	//getters
	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	//setters
	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setRestartCount(int count){
        this.restartCount = count;
    }

    public int getRestartCount(){
        return this.restartCount;
    }

    public String getColon(){
        return this.colon;
    }

    //mark a number
	public String markNum(int num){
        if(this.getNumberStates()[num]==null){
            return (this.getName()+ this.getColon()+"Kartu tidak memiliki angka "+ num);
        }
        else{
            if(this.getNumberStates()[num].isChecked()){
                this.checkBingo();
                return(this.getName()+this.getColon() +num + " sebelumnya sudah tersilang");
            }
            else{
                this.getNumberStates()[num].setChecked(true);
                this.checkBingo();
                return(this.getName()+this.getColon() + num +" tersilang");
            }
        }
    }
	
	//print current state of the board
	public String info(){
        String output = "";
        if (this.getName()!="") output += this.getName()+"\n";
        for(int row = 0;row<this.getNumbers().length;row++){
            for(int col = 0;col<this.getNumbers()[row].length;col++){
                output += "| ";
                if(this.getNumbers()[row][col].isChecked()){
                    output += "X ";
                }
                else{
                    if (this.getNumbers()[row][col].getValue()<10)
                        output += String.format("%2s", 
                            Integer.toString(this.getNumbers()[row][col].getValue())).replace(' ', '0');
                    else
                        output += this.getNumbers()[row][col].getValue();
                }
                output += " ";
            }
            if(row==this.getNumbers().length-1) output += ("|");
            else output += ("|\n");
        }
        return output;
	}
	
	//restart all boards to initial state
	public void restart(){
		for(int row = 0;row<this.getNumbers().length;row++){
            for(int col = 0;col<this.getNumbers()[row].length;col++){
                this.getNumbers()[row][col].setChecked(false);
            }
        }
        System.out.println(this.getName()+this.getColon()+"Mulligan!");
	}
	
	//check if bingo is achieved
    public void checkBingo(){
        int counterX = 0;
        int counterY = 0;
        for(int row = 0;row<this.getNumbers().length;row++){
            counterX = 0;
            for(int col = 0;col<this.getNumbers()[row].length;col++){
                if(this.getNumbers()[row][col].isChecked()){ counterX++; }
                else{ counterX = 0; }
                if(counterX==this.getNumbers().length) this.setBingo(true);
            }
        }
        for(int col = 0;col<this.getNumbers().length;col++){
            counterY = 0;
            for(int row = 0;row<this.getNumbers()[col].length;row++){
                if(this.getNumbers()[row][col].isChecked()){ counterY++; }
                else{ counterY = 0; }
                if(counterY==this.getNumbers().length) this.setBingo(true);
            }
        }

        int counterZ1 = 0; int counterZ2 = 0;
        for(int d=0;d<this.getNumbers().length;d++){
            if(this.getNumbers()[d][d].isChecked()){ counterZ1++; }
            else{ counterZ1 = 0; }

            if(this.getNumbers()[d][this.getNumbers().length-d-1].isChecked()){ counterZ2++; }
            else{ counterZ2 = 0; }

            if((counterZ1==this.getNumbers().length) || (counterZ2==this.getNumbers().length)){ this.setBingo(true); }
        }
        System.out.println(this.isBingo());
        if(this.isBingo()){
            System.out.println(this.getName()+this.getColon()+ "BINGO!");
            System.out.println(this.info());
        }
    }

}
