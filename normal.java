import java.util.Scanner;

public class normal{
	//main function
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		String strType = input.next();

		String strRabbit = input.next().trim(); 

		int n = breed(1,strRabbit.length());

		System.out.println(n); 
	}

	static int breed(int breeds,int breedPotential){
		int total = 0;
		if(breedPotential <= 1)
			return 1;
		else{
			total += breeds*breedPotential + breed(breed*breedPotential,breedPotential-1);
			return total;
		}
	}
}