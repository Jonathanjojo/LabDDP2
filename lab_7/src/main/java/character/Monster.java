package character;

/**
* Class Monster, Subclass dari Player.
* @author  Jonathan Christopher Jakub.
* @since   9-4-2018.
*/
public class Monster extends Player{
    String roar;

    /**
     * Constructor 1 untuk class Monster.
     * @param name string nama monster.
     * @param hp int HP inisial monster.
     */
    public Monster(String name, int hp){
        super(name, 2*hp);
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    }

    /**
     * Constructor 2 untuk class Monster.
     * @param name string nama mons.
     * @param hp int HP inisial monster.
     * @param roar String teriakan
     */
    public Monster(String name, int hp, String roar){
        super(name, 2*hp);
        this.roar = roar;
    }
    /**
     * Roar : berteriak , unik untuk monster saja.
     * @return string teriakan.
     */
    public String roar(){
        return this.roar;
    }
}