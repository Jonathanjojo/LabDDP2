package character;

/**
* Class Human, subclass dari Player
* @author  Jonathan Christopher Jakub.
* @since   8-4-2018.
*/
public class Human extends Player{
    /**
     * Constructor untuk class Human.
     * @param name string nama manusia.
     * @param hp int HP inisial manusia.
     */
    public Human(String name, int hp){
        super(name, hp);
    }
}