package character;

import java.util.ArrayList;

/**
* Abstract Class Player.
* @author  Jonathan Christopher Jakub.
* @since   8-4-2018.
*/
public class Player{

    public static final int DAMAGE = 10;
    public static final int HEAL = 15;

    private String name;
    private int hp;
    private String genre;
    private ArrayList<Player> diet = new ArrayList<Player>();
    private boolean burned;

    /**
     * Constructor untuk class Player.
     * @param name string nama pemain.
     * @param hp int HP inisial pemain.
     */
    public Player(String name, int hp){
        this.name = name;
        this.setHp(hp);
        if(this instanceof Magician){
            this.genre = "Magician";
        } else if (this instanceof Human){
            this.genre = "Human";
        } else {
            this.genre = "Monster";
        }
    }

    /**
     * Setters and Getters
     */
    public String getName(){
        return this.name;
    }

    public void setHp(int hp){
        if (hp < 0){
            this.hp = 0;
        } else {
            this.hp = hp;
        }
    }

    public int getHp(){
        return this.hp;
    }

    public ArrayList<Player> getDiet(){
        return this.diet;
    }

    public boolean isDead(){
        if (this.getHp()==0){
            return true;
        } else {
            return false;
        }
    }

    public void setBurn(boolean burned){
        this.burned = burned;
    }

    public boolean isBurned(){
        return this.burned;
    }

    public String getGenre(){
        return this.genre;
    }


     /**
     * Menyerang dengan damage 10 
     * @param victim Player target yang di burn
     * @return string sesuai kriteria soal
     */
    public String attack(Player victim){
        if(victim instanceof Magician){
            victim.setHp(victim.getHp() - 2*DAMAGE);
        } else {
            victim.setHp(victim.getHp() - DAMAGE);
        }
        return "Nyawa " + victim.getName() + " " + victim.getHp();
    }

    /**
     * Penentu apakah player lain bisa dimakan atau tidak.
     * @param food player lain yang akan dimakan.
     * @return String sesuai kriteria soal.
     */
    public boolean canEat(Player food){
        if(this instanceof Human){
            if((food.isBurned()) && (food.isDead()) && (food instanceof Monster)){
                return true;
            } else {
                return false;
            }
        } else {
            if((food.isBurned()) && (food.isDead())) {
                return true;
            } else {
                return false;
            }
        }
    } 

    /**
     * Memakan player lain jika bisa dimakan.
     * @see canEat.
     * @param foo player lain yang akan dimakan.
     * @return string sesuai kriteria soal.
     */
    public String[] eat(Player food){
        String[] out = new String[2];
        if (this.canEat(food)){
            this.setHp(this.getHp() + HEAL);
            this.getDiet().add(food);
            out[0] = this.getName() + " memakan " + food.getName() + "\n" 
                + "Nyawa " + this.getName() + " kini " + this.getHp();
            out[1] = "eaten";
        } else {
            out[0] = this.getName() + " tidak bisa memakan " + food.getName();
            out[1] = "not eaten";
        }
        return out;
    }
}