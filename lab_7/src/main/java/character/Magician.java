package character;

/**
* Class Magician, subclass dari Human
* @author  Jonathan Christopher Jakub.
* @since   8-4-2018.
*/
public class Magician extends Human{
    /**
     * Constructor untuk class Magician.
     * @param name string nama penyihir.
     * @param hp int HP inisial penyihir.
     */
    public Magician(String name, int hp){
        super(name, hp);
    }

    /**
     * Tipe attack unik : burn
     * Burn memiliki damage 10
     * Burn dapat mematangkan mahkluk yang sudah mati sehingga bisa dimakan
     * @param victim Player target yang di burn
     * @return string sesuai kriteria soal
     */
    public String burn(Player victim){
        if (victim.getHp() - Player.DAMAGE <= 0 ){
            victim.setBurn(true);
            return this.attack(victim) + "\ndan matang";
        } else {
            return this.attack(victim);
        }
        
    }
}