import character.*;
import java.util.ArrayList;

/**
* Merupakan class pengatur game RPG
* Class Game
* @author  dari ASDOS, modifikasi oleh : Jonathan Christopher Jakub.
*/
public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player character object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player p: player){
            if(p.getName().equals(name)){
                return p;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara) != null){
            return "Sudah ada karakter bernama "+chara;
        }
        if (tipe.equals("Human")){
            player.add(new Human(chara,hp));
        } else if (tipe.equals("Magician")){
            player.add(new Magician(chara,hp));
        } else if (tipe.equals("Monster")){
            player.add(new Monster(chara,hp));
        }
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if (find(chara) != null){
            return "Sudah ada karakter bernama "+chara;
        }
        if (tipe.equals("Human") || tipe.equals("Magician")){
            return "Roar hanya bisa dilakukan oleh monster";
        } else if (tipe.equals("Monster")){
            player.add(new Monster(chara,hp,roar));
        }
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if (find(chara) == null){
            return "Tidak ada "+chara;
        } else {
            player.remove(find(chara));
            return chara + " dihapus dari game";
        }
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        String output = "";
        if (find(chara) == null){
            return "Tidak ada "+chara;
        } else {
            output += find(chara).getGenre()+ " " + find(chara).getName() + "\n"
                + "Hp: " + find(chara).getHp() + "\n";
            if(find(chara).isDead()){
                output += "Sudah meninggal dunia dengan damai\n";
            } else {
                output += "Masih hidup\n";
            }
            if(find(chara).getDiet().size()==0){
                output += "Belum memakan siapa siapa";
            } else {
                output += "Memakan ";
                for (Player p: find(chara).getDiet()){
                    output += p.getGenre() + " " + p.getName() + " ";
                }
            }
        } 
        return output.trim();
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String output = "";
        for (Player p : player){
            output += this.status(p.getName()) + "\n";
        }     
        return output.trim();
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        if (find(chara) == null){
            return "Tidak ada "+chara;
        } else {
            String diets = "";
            for (Player f: find(chara).getDiet()){
                diets += f.getGenre()+" "+f.getName()+" ";
            }
            return diets.trim();
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String output = "";
        for (Player p : player){
            output += this.diet(p.getName()) + "\n";
        }     
        return output.trim();
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        if (find(meName) == null){
            return "Tidak ada "+ meName;
        } else if (find(enemyName) == null){
            return "Tidak ada "+ enemyName;
        } else {
            return find(meName).attack(find(enemyName));
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        if (find(meName) == null){
            return "Tidak ada "+ meName;
        } else if (find(enemyName) == null){
            return "Tidak ada "+ enemyName;
        } else {
            if (find(meName) instanceof Magician){
                return ((Magician)(find(meName))).burn(find(enemyName));
            }
            return meName + " tidak bisa membakar " + enemyName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        String[] out = new String[2];
        if (find(meName) == null){
            return "Tidak ada "+ meName;
        } else if (find(enemyName) == null){
            return "Tidak ada "+ enemyName;
        } else {
            out = find(meName).eat(find(enemyName));
            if (out[1].equals("eaten")){
                this.remove(enemyName);
            }
            return out[0];
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        if (find(meName) == null){
            return "Tidak ada "+ meName;
        } else {
            if(find(meName) instanceof Monster){
                return ((Monster)(find(meName))).roar();
            } else {
                return meName + " tidak bisa berteriak";
            }
        }
    }
}