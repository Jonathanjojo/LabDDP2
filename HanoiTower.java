import java.util.Scanner;

public class HanoiTower{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		String src = "SOURCE";
		String aux = "AUXILIARY";
		String dst = "DESTINATION";
		System.out.print("Number of disks: ");
		int n = input.nextInt();
		stack(n,src,aux,dst);
	}

	public static void stack(int n, String src, String aux, String dst){
		if(n==1){
			//Output current state
			System.out.println("Move disk "+n+" from "+src+" to "+dst);
			//Base case
		}
		else{
			stack(n-1, src, dst, aux);
			//Move a tower of height-1 to aux tower using the dst tower
			System.out.println("Move disk "+n+" from "+src+" to "+dst);
			//Move CURRENT base disk to dst
			stack(n-1, aux, src, dst);
			//Move the tower of height-1 from the aux tower to the dst tower using the src tower
		}
	}
}