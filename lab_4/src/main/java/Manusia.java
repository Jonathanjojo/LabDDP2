/**
*@author Jonathan Christopher Jakub, NPM 1706040151, Kelas DDP2-D, GitLab Account: @Jonathanjojo
*OOP Implementation : Manusia
**/

//importing modules
import java.util.List;
import java.util.ArrayList;

//building class
public class Manusia{
	//shared attributes of class Manusia's objects
	private String nama;
	private int umur;
	private long uang;
	private float kebahagiaan;
	private boolean hidup;
	private String gelarHidup;
	private static Manusia manusiaTerakhir;
	
	//constructor 1
	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
		this.hidup = true;
		this.gelarHidup = "";
		this.manusiaTerakhir = this;
	} 

	//constructor 2
	public Manusia(String nama, int umur, long uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
		this.hidup = true;
		this.gelarHidup = "";
		this.manusiaTerakhir = this;	}

	//validator: alive or passed (passed person are not able to do anything)
	private boolean validasiHidup(){
		if(this.getHidup()==true){
			return true;
		}
		else{
			System.out.println(this.getNama()+" telah tiada");
			return false;
		}
	}

	//item setters
	void setNama(String nama){ this.nama = nama; }
	void setUmur(int umur){ this.umur = umur; }
	void setUang(long uang){ this.uang = uang; }
	void setKebahagiaan(float kebahagiaan){ 
		if (kebahagiaan>100){
			this.kebahagiaan = 100;}
		else if(kebahagiaan<0){
			this.kebahagiaan = 0;}
		else{
			this.kebahagiaan = kebahagiaan; }
	}
	void setHidup(boolean hidup){ this.hidup = hidup; }
	void setGelarHidup(String gelar){this.gelarHidup = gelar;}

	//item getters
	String getNama(){ return this.nama; }
	int getUmur(){ return this.umur; }
	long getUang(){ return this.uang; }
	float getKebahagiaan(){ return this.kebahagiaan; }
	boolean getHidup() { return this.hidup; }
	String getGelarHidup(){return this.gelarHidup;}

	//method : beriUang() 2
	public void beriUang(Manusia penerima){
		if(this.validasiHidup() && penerima.validasiHidup()){
			int kiriman = 0;
			for(int c=0;c<penerima.getNama().length();c++){
				kiriman += (int)(penerima.getNama().charAt(c));
			}
			kiriman *= 100;
			//if sender has enough credit
			if(this.getUang()<kiriman){
				System.out.println(
					this.getNama()+" ingin memberi uang kepada "+
					penerima.getNama()+ " namun tidak memiliki cukup uang :'(");
			}
			//if sender does not have enough credit
			else{
				this.setUang(this.getUang()-kiriman);
				penerima.setUang(penerima.getUang()+kiriman);
				this.setKebahagiaan(this.getKebahagiaan() + (float)(kiriman/6000.0));
				penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)(kiriman/6000.0));
				System.out.println(
					this.getNama()+" memberi uang sebanyak "+kiriman+" kepada "+
					penerima.getNama()+", mereka berdua senang :D");
			}
		}
	}

	//method: beriUang() 2
	public void beriUang(Manusia penerima, long kiriman){
		if(this.validasiHidup()){
			//if sender has enough credit
			if(this.getUang()<kiriman){
				System.out.println(
					this.getNama()+" ingin memberi uang kepada "+
					penerima.getNama()+ " namun tidak memiliki cukup uang :'(");
			}
			//if sender does not have enough credit
			else{
				this.setUang(this.getUang() - kiriman);
				penerima.setUang(penerima.getUang() + kiriman);
				this.setKebahagiaan(this.getKebahagiaan() + (float)(kiriman/6000.0));
				penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)(kiriman/6000.0));
				System.out.println(
					this.getNama()+" memberi uang sebanyak "+kiriman+" kepada "+
					penerima.getNama()+", mereka berdua senang :D");	
			}
		}
	}
	
	//method: bekerja()
	public void bekerja(int durasi, int bebanKerja){
		if(this.validasiHidup()){
			//special condition: age above 18
			if(this.getUmur()<18){
				System.out.println(this.getNama()+" belum boleh bekerja karena masih dibawah umur D:");
			}
			else{
				long pendapatan = 0;
				int bebanKerjaTotal = durasi * bebanKerja;
				//case when workload <= happiness level
				if(bebanKerjaTotal<=this.getKebahagiaan()){
					this.setKebahagiaan(this.getKebahagiaan()-bebanKerjaTotal);
					pendapatan = bebanKerjaTotal * 10000;
					System.out.println(this.getNama()+" bekerja full time, total pendapatan : "+pendapatan);
				}
				//case when workload > happiness level
				else{
					int durasiBaru = (int)(this.getKebahagiaan()/bebanKerja);
					bebanKerjaTotal = durasiBaru * bebanKerja;
					pendapatan = bebanKerjaTotal * 10000;
					this.setKebahagiaan(this.getKebahagiaan()-bebanKerjaTotal);
					System.out.println(this.getNama()+" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
				}
				//objects get paid for the work
				this.setUang(this.getUang() + pendapatan);
			}
		}
	}

	//gaining happiness through activity (costs credits)
	public void rekreasi(String namaTempat){
		if(this.validasiHidup()){
			long biaya = namaTempat.length() * 10000;
			if(this.getUang()>=biaya){
				this.setUang(this.getUang()-biaya);
				this.setKebahagiaan(this.getKebahagiaan() + namaTempat.length());
				System.out.println(this.getNama()+ " berekreasi di "+namaTempat+", "+this.getNama()+" senang :)");
			}
			else{
				System.out.println(this.getNama()+" tidak mempunyai cukup uang untuk berekreasi di "+namaTempat+" :(");
			}
		}
	}

	//object got sick (lower happiness level)
	public void sakit(String namaPenyakit){
		if(this.validasiHidup()){
			this.setKebahagiaan(this.getKebahagiaan() - namaPenyakit.length());
			System.out.println(this.getNama()+ " terkena penyakit "+namaPenyakit+" :O");
		}
	}

	//info printer
	public String toString(){
		return(
			"Nama        : " + this.getGelarHidup() + this.getNama() + "\n" +
			"Umur        : " + this.getUmur() + "\n" +
			"Uang        : " + this.getUang() + "\n" +
			"Kebahagiaan : " + this.getKebahagiaan());
	}

	//object dies (object is not destroyed)
	public void meninggal(){
		if(this.validasiHidup()){
			this.setHidup(false);
			System.out.println(this.getNama()+" meninggal dengan tenang, kebahagiaan: "+this.getKebahagiaan());	
			this.setGelarHidup("Almarhum ");
			//remaining credits of passed away object goes to the last human object created

			if(this.manusiaTerakhir.getHidup())	{
				this.manusiaTerakhir.setUang(this.manusiaTerakhir.getUang()+ this.getUang());
				this.setUang(0);
				System.out.println("Semua harta "+this.getNama()+" disumbangkan untuk "+(this.manusiaTerakhir.getNama()));
			} else{
				this.setUang(0);
				System.out.println("Semua harta "+this.getNama()+" hangus");
			}
		}
	}
}